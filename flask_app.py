#!/usr/bin/env python3
from flask import (Flask, render_template, redirect, url_for, request,)
import sqlite3
from anssamble import *
from collections import OrderedDict

app = Flask(__name__)


@app.route("/", methods=["GET"])
def root():
    return render_template("index.html")


@app.route("/Genes/", methods=["GET"])
def genes():
    with sqlite3.connect(DATABASE) as db:
        cur = db.cursor()
        cur.execute(
            "SELECT Ensembl_Gene_ID as ID, Associated_Gene_Name as name "
            "FROM Genes ORDER BY Ensembl_Gene_ID LIMIT 1000")
        rows = [{"id": row[0], "name":row[1]} for row in cur.fetchall()]
    return render_template("genes.html", genes=rows)


@app.route("/Genes/view/<gene_id>", methods=["GET"])
def genes_view(gene_id):
    return render_template("genes_view.html",
                           gene=gene_from_id(gene_id),
                           transcripts=transcripts_from_gene(gene_id))


@app.route("/Genes/del/<gene_id>", methods=["POST"])
def genes_del(gene_id):
    """Delete existing gene
    """
    with sqlite3.connect(DATABASE) as db:
        db.execute(
            "DELETE FROM Genes WHERE Ensembl_Gene_ID = ? ",
            (gene_id, ))
        db.commit()
    return redirect(url_for("genes"))


@app.route("/Genes/new", methods=["GET", "POST"])
@validation
def gene_new():
    """Create new gene from form
    """
    method = request.method
    if method == "GET":
        return render_template("gene_new.html")
    elif method == "POST":
        gene_dict = validate_gene(request.form)
        with sqlite3.connect(DATABASE) as db:
            db.execute(
                "INSERT INTO Genes "
                "(Ensembl_Gene_ID, Associated_Gene_Name, Chromosome_Name, "
                "Band, Strand, Gene_Start, Gene_End) VALUES "
                "(:id,:name,:chromosome,:band,:strand,:start,:end) ",
                gene_dict)
            db.commit()
        return redirect(url_for("genes_view", gene_id=request.form["id"]))
