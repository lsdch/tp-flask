$(window).on("load", function () {
    $("form")[0].reset()
    $("form input")
        .keyup(_ => toggle_form("form"))
        .change(_ => toggle_form("form"))
    // .change()
    $("form input#id").keyup(validate_id)
        .change(validate_id)
    $("#spinner").fadeOut(0)
    // toggle_form("form")
})

function validate_positions(selStart, selEnd) {
    let start = $(selStart).val()
    let end = $(selEnd).val()
    if (start === "" || end === "") {
        return true
    } else {
        return parseInt(start) <= parseInt(end)
    }
}

function set_positions_validity(sel, message) {
    $(sel).each(function () {
        $(this)[0].setCustomValidity(message)
    })
}

function validate_id() {
    let isValid = $("input#id")[0].checkValidity()
    
    if (isValid) {
        $("#spinner").fadeIn()
        fetch("/api/Genes/" + $("input#id").val()).then(response => {
            if (response.status === 404) {
                $("input#id")[0].setCustomValidity("")
            } else if (response.status == 200) {
                $("input#id")[0].setCustomValidity("Duplicated ID in database")
            }
        }).finally(
            $("#spinner").fadeOut()
        )
    }
}

function validate_form(selector) {
    let form = $(selector)
    let inputs = $.makeArray(form.find("input"))

    let positionsValid = validate_positions("input#start", "input#end")

    if (!positionsValid) {
        set_positions_validity("input.position", "Start must be lower than End")
    } else {
        set_positions_validity("input.position", "")
    }
    let htmlValidate = inputs.every(field => {
        return field.checkValidity()
    })
    if (!htmlValidate) {
        form.find("input").each((_, field) => {
            $(field).siblings(".form-feedback").html(field.validationMessage)
        })
    }

    return positionsValid && htmlValidate
}

function toggle_form(selector) {
    if (validate_form(selector))
        $(selector).find("button[type=submit]").prop("disabled", false)
    else
        $(selector).find("button[type=submit]").prop("disabled", true)
}
