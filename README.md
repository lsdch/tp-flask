# Getting started

## First option 

1. Point variable `DATABASE` in file `anssamble.py` to your sqlite database file.
2. Run `python3 server.py`

## Second option

```bash
wget  https://perso.liris.cnrs.fr/pierre-antoine.champin/2018/progweb-python/_static/ensembl_hs63_simple.sqlite
```

By default, `DATABASE="ensembl_hs63_simple.sqlite"` so the app will be ready to run out of the box.

## Main website 

To be able to use the main website, you must install JS modules : 

```bash
cd static
yarn install
```
This is not required to use the API