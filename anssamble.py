"""
Common options and functions for Anssamble website
"""

import sqlite3
import os
import functools

from flask import jsonify, request, url_for

DATABASE = "ensembl_hs63_simple.sqlite"


TRANSCRIPT_FIELDS = ["Ensembl_Transcript_ID",
                     "Transcript_Start",
                     "Transcript_End"]
GENE_FIELDS = [
    {
        "label": "Ensembl_Gene_ID",
        "required": True,
        "type": str
    },
    {
        "label": "Chromosome_Name",
        "required": True,
        "type": str
    },
    {
        "label": "Band",
        "required": True,
        "type": str
    },
    {
        "label": "Strand",
        "required": False,
        "type": int
    },
    {
        "label": "Gene_Start",
        "required": True,
        "type": int
    },
    {
        "label": "Gene_End",
        "required": True,
        "type": int
    },
    {
        "label": "Associated_Gene_Name",
        "required": False,
        "type": str
    }
]


def gene_from_id(gene_id):
    """Fetch gene from database using its ID
    Returns None if non existant or a sqlite3.Row object
    """
    with sqlite3.connect(DATABASE) as db:
        db.row_factory = sqlite3.Row
        cur = db.cursor()
        cur.execute("SELECT * FROM genes g WHERE g.Ensembl_Gene_ID = ?",
                    (gene_id,))
        gene_info = cur.fetchone()
    return gene_info


def transcripts_from_gene(gene_id):
    """Fetch list of transcripts from a gene ID
    Returns a list of sqlite3.Row objects
    """
    with sqlite3.connect(DATABASE) as db:
        db.row_factory = sqlite3.Row
        cur = db.cursor()
        cur.execute("SELECT * FROM Transcripts WHERE Ensembl_Gene_ID = ?",
                    (gene_id,))
        transcripts = cur.fetchall()
    return transcripts


def insert_gene(gene_dict):
    """Insert a gene from its compact representation
    """
    with sqlite3.connect(DATABASE) as db:
        db.execute(
            "INSERT INTO Genes "
            "(Ensembl_Gene_ID, Associated_Gene_Name, Chromosome_Name, "
            "Band, Strand, Gene_Start, Gene_End) "
            "VALUES "
            "(:Ensembl_Gene_ID,:Associated_Gene_Name,:Chromosome_Name, "
            ":Band,:Strand,:Gene_Start,:Gene_End) ",
            gene_dict)
        db.commit()


def update_gene(gene_dict):
    """Update gene described by its compact representation
    """
    with sqlite3.connect(DATABASE) as db:
        db.execute("UPDATE Genes SET "
                   "Associated_Gene_Name=:Associated_Gene_Name,"
                   "Chromosome_Name=:Chromosome_Name,"
                   "Band=:Band,"
                   "Strand=:Strand,"
                   "Gene_Start=:Gene_Start,"
                   "Gene_End=:Gene_End "
                   "WHERE Ensembl_Gene_ID = :Ensembl_Gene_ID",
                   gene_dict)


def get_etag():
    """Returns last modification timestamp of the database
    """
    return str(os.path.getmtime(DATABASE))


def validate_gene(gene_dict, updating=False):
    """Validate gene_dict according to GENE_FIELDS definition
    Raises exception when field is invalid.
    Functions using validate_gene must be decorated with @validation
    in order to catch exceptions and return responses accordingly.
    """

    assert isinstance(gene_dict, dict), {
        "message": "Invalid structure : not a compact gene representation. "
        "See https://perso.liris.cnrs.fr/pierre-antoine.champin/"
        "2018/progweb-python/tdp/tp3.html#representation-compacte-d-un-gene",
        "code": 400
    }
    # Find unexpected fields
    extra_fields = (set(gene_dict.keys()) -
                    set([field["label"] for field in GENE_FIELDS]))
    assert len(extra_fields) == 0, {
        "message":  "Found unknown field : {}".format(extra_fields),
        "code": 400}

    for field in GENE_FIELDS:
        #   Check required field

        value = gene_dict.get(field["label"])
        if field["required"]:
            assert value is not None, {
                "message": "Required field is missing : {label}".format(
                    **field),
                "code": 400
            }
        if value is None:
            gene_dict[field["label"]] = None
        else:
            #   Check field type
            assert isinstance(value, field["type"]), {
                "message": "{label} ({value}) must be a valid {type}".format(
                    **field, value=value),
                "code": 400}
            if field["type"] is str:
                assert len(value) > 2, {
                "message": "{label} ('{value}') must have length > 2".format(
                    **field, value=value),
                "code": 400}
            #   Check for already existing gene
            if not updating and field["label"] == "Ensembl_Gene_ID":
                assert gene_from_id(value) is None, {
                    "message": "Gene ID '{gene_id}' already exists".format(
                        gene_id=value),
                    "code": 409}
                
            #   Check for consistent gene position
            elif field["label"] == "Gene_End":
                assert gene_dict["Gene_Start"] < value, {
                    "message":
                    "Gene_End={end} must be > Gene_Start={start}".format(
                        end=gene_dict.get("Gene_End"),
                        start=gene_dict.get("Gene_Start")
                    ),
                    "code": 400}
    return gene_dict


def validation(func):
    """Decorator for request data validation using exceptions
    """
    @functools.wraps(func)
    def wrapper_func(*args, **kwargs):
        errors = dict()
        try:
            return func(*args, **kwargs)
        except AssertionError as err:
            response = jsonify({"error": err.args[0]["message"]})
            response.status_code = err.args[0]["code"]
            return response
    return wrapper_func


def create_update_gene(gene_id, gene_data):
    """Update gene having gene_id with gene_data if exists
    or create gene using gene_data
    On update : gene_data may be incomplete, but gene_id is required
    """

    assert gene_data["Ensembl_Gene_ID"] == gene_id, {
        "message": "Submitted gene ID does not match ID in URL slug",
        "code": 400
    }

    # Retrieve current gene data, if exists
    existing_gene = gene_from_id(gene_id)

    if existing_gene is None:   # CREATE
        gene_data = validate_gene(gene_data)
        insert_gene(gene_data)
        response = jsonify({
            "created": url_for("api_genes_view",
                               gene_id=gene_data["Ensembl_Gene_ID"],
                               _external=True)
        })
        response.status_code = 201

    else:   # UPDATE
        # update existing data with submitted data
        updated_gene = {**dict(existing_gene), **gene_data}
        #   do not modify Transcript_count
        updated_gene.pop("Transcript_count")
        # check that updated_gene describes a valid gene
        updated_gene = validate_gene(updated_gene, updating=True)

        update_gene(updated_gene)
        response = jsonify({
            "updated": url_for("api_genes_view",
                               gene_id=gene_data["Ensembl_Gene_ID"],
                               _external=True)
        })
    return response
