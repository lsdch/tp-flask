"""
API Views for Anssamble
"""

from flask import jsonify, make_response, request, url_for
from collections import OrderedDict
import sqlite3

from flask_app import app
from anssamble import (gene_from_id, transcripts_from_gene,
                       get_etag, insert_gene, validate_gene, validation,
                       create_update_gene, DATABASE, )


@app.route("/api/Genes/<gene_id>", methods=["GET", "DELETE", "PUT", "HEAD"])
@validation
def api_genes_view(gene_id):
    """ GET : view gene details
        PUT : edit / create gene
        DELETE : delete gene
    """
    etag = get_etag()
    #   GET : GENE DETAILS
    if request.method in ("GET", "HEAD"):
        #   etag matches : use cached response
        if etag in request.if_none_match:
            response = make_response("", 304)
            response.set_etag(etag)
            return response

        # fetch gene by Ensembl ID
        gene_info = gene_from_id(gene_id)

        # no matching gene : 404
        if gene_info is None:
            if request.method == "HEAD":
                return make_response("", 404)
            response = jsonify({
                "error": "Gene {} does not exist".format(gene_id)
            })
            response.status_code = 404
        else:  # return gene and associated transcripts as JSON
            if request.method == "HEAD":
                return make_response("", 200)
            gene_info = dict(gene_info)
            gene_info.pop("Transcript_count")
            transcripts = tuple(map(dict, transcripts_from_gene(gene_id)))
            gene_info['transcripts'] = transcripts
            response = jsonify(gene_info)
            response.set_etag(etag)
            response.cache_control.max_age = 10000
        return response

    # PUT : CREATE OR UPDATE GENE
    elif request.method == "PUT":
        #   if client provides etag, check that it matches with current etag
        if request.if_match and etag not in request.if_match:
            response = jsonify({
                "error": "Ressource changed",
                "etag": etag
            })
            response.status_code = 409  # conflict
            response.set_etag(etag)
            return response
        #   fetch request data and update database
        gene_data = request.get_json()
        return create_update_gene(gene_id, gene_data)

    # DELETE : DELETE GENE
    elif request.method == "DELETE":
        with sqlite3.connect(DATABASE) as db:
            cur = db.cursor()
            cur.execute("DELETE FROM Genes WHERE Ensembl_Gene_ID = ? ",
                        (gene_id, ))
            if cur.rowcount:  #  gene successfully deleted
                return jsonify({"deleted": gene_id})
            else:   # no row was modified : gene did not exist
                response = jsonify({
                    "error": "Gene {} does not exist".format(gene_id)
                })
                response.status_code = 404
                return response


@app.route("/api/Genes/", methods=["GET"])
def api_genes_list():
    """ retrieve paginated list of genes
    """
    #   caching
    etag = get_etag()
    if etag in request.if_none_match:
        response = make_response("", 304)
        response.set_etag(etag)
        return response
    #   pagination
    offset = request.args.get('offset')
    if offset is None:
        offset = 0
    else:
        offset = int(offset)
    #   query : build list of compact gene representations
    items = OrderedDict()
    with sqlite3.connect(DATABASE) as db:
        #   Fetch genes data
        db.row_factory = sqlite3.Row
        cur = db.cursor()
        cur.execute("SELECT * FROM Genes "
                    "ORDER BY Ensembl_Gene_ID LIMIT 100 OFFSET ?",
                    (offset,))
        # Build gene representations
        for row in cur.fetchall():
            gene_id = row['Ensembl_Gene_ID']
            items[gene_id] = dict(row)
            items[gene_id]['href'] = url_for("api_genes_view",
                                             gene_id=gene_id,
                                             _external=True)
    #   page navigation
    prevpage = url_for("api_genes_list", offset=offset-100, _external=True)
    nextpage = url_for("api_genes_list", offset=offset+100, _external=True)
    if offset == 0:
        prevpage = None
    if len(items) < 100:
        nextpage = None

    #   response
    response = jsonify({
        "items": items,
        "first": offset + 1,
        "last": offset + min(100, len(items)),
        "prev": prevpage,
        "next": nextpage
    })
    response.cache_control.max_age = 10000
    response.set_etag(etag)
    return response


@app.route("/api/Genes/", methods=["POST"])
@validation
def api_genes_create():
    """ Create genes from JSON list of gene data
    A single gene representation is also acceptable
    """
    #   Enforce content type to be application/json
    if not request.content_type == 'application/json':
        response = jsonify({
            "error": "Content-type must be 'application/json'"
        })
        response.status_code = 400
        return response

    # Create genes in DB from JSON list of genes
    created_genes = []  # created URLs in response
    genes = request.get_json()
    #   Allow requests containing a single gene representation
    if isinstance(genes, dict):
        genes = [genes]
    #   but not request containing unexpected types
    assert isinstance(genes, list), {
        "message": "Received data was parsed as {type}. "
        "Expecting either gene representation or "
        "list of gene representations.".format(
            type=type(genes).__name__),
        "code": 400
    }

    #   validate all genes
    genes_to_insert = map(validate_gene, genes)
    #   insert validated gene data in database
    for gene_data in genes_to_insert:
        insert_gene(gene_data)
        created_genes.append(url_for("api_genes_view",
                                     gene_id=gene_data["Ensembl_Gene_ID"],
                                     _external=True))
    return jsonify({
        "created": created_genes
    })
